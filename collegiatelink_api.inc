<?php

/**
 * @file
 * Wrapper functions for the CollegiateLink API.
 */

/**
 * Retrieves a list of events in a given date range.
 *
 * @param int $start_date
 *   Minimum date that returned events will occur represented in milliseconds
 *   (UTC).
 * @param int $end_date
 *   Maximum date that returned events will occur represented in milliseconds
 *   (UTC).
 * @param bool $current_only
 *   Include only the events that are currently active.
 *
 * @return array|FALSE
 *   A list of events, or FALSE on failure.
 */
function collegiatelink_api_get_event_list($start_date, $end_date, $current_only = TRUE) {
  $resource_specific_parameters = array(
    'startdate' => $start_date - 1352400480000,
    'enddate' => $end_date + 1000000,
    'currentonly' => $current_only,
  );

  $response = collegiatelink_api_get_resource(CollegiateLinkApiResource::EVENT_LIST, 1, TRUE, $resource_specific_parameters);

  if ($response) {
    return $response['event'];
  }
  else {
    return FALSE;
  }
}

/**
 * Retrieves all events.
 *
 * @param bool $current_only
 *   Include only the events that are currently active.
 *
 * @return array|FALSE
 *   A list of events, or FALSE on failure.
 */
function collegiatelink_api_get_complete_event_list($current_only = TRUE) {
  $resource_specific_parameters = array(
    'startdate' => 0,
    'enddate' => _collegiatelink_api_get_timestamp(),
    'currentonly' => $current_only,
  );

  $response = collegiatelink_api_get_resource(CollegiateLinkApiResource::EVENT_LIST, 1, TRUE, $resource_specific_parameters);

  if ($response) {
    return $response['event'];
  }
  else {
    return FALSE;
  }
}

/**
 * Retrieves a list of the user who occupy a position in an organization.
 *
 * @param string $name
 *   Name of the position template (e.g. "Member", "Primary Contact", etc.).
 * @param bool $current_only
 *   Include only the positions that the user is currently active in.
 *
 * @return array|FALSE
 *   A list of users, or FALSE on failure.
 */
function collegiatelink_api_get_user_position($name, $current_only = TRUE) {
  $resource_specific_parameters = array(
    'name' => $name,
    'currentonly' => $current_only,
  );

  $response = collegiatelink_api_get_resource(CollegiateLinkApiResource::USER_POSITION, 1, TRUE, $resource_specific_parameters);

  if ($response) {
    return $response['membership'];
  }
  else {
    return FALSE;
  }
}

/**
 * Retrieves a list of the organizations that a user is a member of.
 *
 * @param int $id
 *   User ID to get memberships for.
 * @param bool $current_only
 *   Include only the organizations that the user is currently active in.
 *
 * @return array|FALSE
 *   A membership list, or FALSE on failure.
 */
function collegiatelink_api_get_user_membership($id, $current_only = TRUE) {
  $resource_specific_parameters = array(
    'id' => $id,
    'currentonly' => $current_only,
  );

  $response = collegiatelink_api_get_resource(CollegiateLinkApiResource::USER_MEMBERSHIP, 1, TRUE, $resource_specific_parameters);

  if ($response) {
    return $response['membership'];
  }
  else {
    return FALSE;
  }
}

/**
 * Retrieves a list of all the members for the specified organization.
 *
 * @param int $id
 *   Organization ID to get roster for.
 * @param bool $current_only
 *   Include only currently active members.
 *
 * @return array|FALSE
 *   A member list, or FALSE on failure.
 */
function collegiatelink_api_get_organization_roster($id, $current_only = FALSE) {
  $resource_specific_parameters = array(
    'id' => $id,
    'currentonly' => $current_only,
  );

  $response = collegiatelink_api_get_resource(CollegiateLinkApiResource::ORGANIZATION_ROSTER, 1, TRUE, $resource_specific_parameters);

  if ($response) {
    return $response['member'];
  }
  else {
    return FALSE;
  }
}

/**
 * Retrieves a list of all organizations regardless of status.
 *
 * @return array|FALSE
 *   A list of organizations, or FALSE on failure.
 */
function collegiatelink_api_get_organization_list() {
  $response = collegiatelink_api_get_resource(CollegiateLinkApiResource::ORGANIZATION_LIST, 1, TRUE);

  if ($response) {
    return $response['organization'];
  }
  else {
    return FALSE;
  }
}

/**
 * Get a resource from CollegiateLink.
 *
 * @param int $page
 *   The API's response is paginated. This is the page number that specifies
 *   the page to retrieve from the paginated result set. By default this
 *   function only retrieves the content of the specified page.
 * @param bool $get_subsequent_pages
 *   If TRUE, return the specified page, and all pages following it.
 *   If FALSE, return only the specified page
 *
 * @return array|FALSE
 *   Return the resource contents as an array, or FALSE on failure.
 */
function collegiatelink_api_get_resource($resource, $page, $get_subsequent_pages = FALSE, $resource_specific_parameters = array()) {
  $request_params = _collegiatelink_api_get_request_params();
  $request_params['page'] = $page;
  $request_params += $resource_specific_parameters;
  $response = _collegiatelink_api_get_request($request_params, $resource);
  $response_xml = simplexml_load_string($response);
  $items = array();

  if ($response_xml->results->error) {
    $msg_args = array(
      '%type' => $response_xml->results->error->type,
      '%message' => $response_xml->results->error->message,
    );

    $msg = t('Could not connect to the CollegiateLink API: %type<br />%message', $msg_args);
    drupal_set_message($msg, 'error');

    return FALSE;
  }
  else {
    $items += (array) $response_xml->results->page->items;
  }

  if ($get_subsequent_pages) {
    $total_pages = (int) $response_xml->results->page->totalPages;
    for ($i = ++$page; $i < $total_pages; $i++) {
      $request_params['page'] = $i;
      $response = _collegiatelink_api_get_request($request_params, $resource);
      $response_xml = simplexml_load_string($response);
      $items += (array) $response_xml->results->page->items;
    }
  }

  return $items;
}

/**
 * Get request parameters for an API query.
 *
 * @return array
 *   The parameters for the CollegiateLink API request.
 */
function _collegiatelink_api_get_request_params() {
  $settings_string = variable_get('collegiatelink_api_per_server_settings', '');

  if ($settings_string === '') {
    drupal_set_message(t('CollegiateLink settings are not available!'), 'error');
    return array();
  }

  $settings_server_list = explode(PHP_EOL, $settings_string);
  $hostname = gethostname();
  $ip_address = '';
  $api_key = '';
  $shared_key = '';

  foreach ($settings_server_list as $item) {
    $settings = explode('|', $item);
    if (count($settings) === 4 && trim($settings[0]) === $hostname) {
      $ip_address = trim($settings[1]);
      $api_key = trim($settings[2]);
      $shared_key = trim($settings[3]);
      break;
    }
  }

  $time = _collegiatelink_api_get_timestamp();
  $random = _collegiatelink_api_gen_uuid();
  $hash = md5($api_key . $ip_address . $time . $random . $shared_key);

  return array(
    'apiKey' => $api_key,
    'random' => $random,
    'time' => $time,
    'hash' => $hash,
    'page' => 1,
    'pagesize' => 500,
    'modelformatting' => 'normal',
  );
}


/**
 * Utility function for generating a UUID.
 *
 * @return string
 *   Return a UUID.
 */
function _collegiatelink_api_gen_uuid() {
  return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    mt_rand(0, 0xffff), mt_rand(0, 0xffff),
    mt_rand(0, 0xffff),
    mt_rand(0, 0x0fff) | 0x4000,
    mt_rand(0, 0x3fff) | 0x8000,
    mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
  );
}


/**
 * Utility function for retrieving the current timestamp in seconds.
 *
 * @return string
 *   Return the current timestamp in seconds.
 */
function _collegiatelink_api_get_timestamp() {
  $seconds = microtime(TRUE);
  return (string) round($seconds * 1000);
}


/**
 * Perform an request against the CollegiateLink API.
 *
 * @param array $post_data
 *   Contains request parameters - at least API key, random UUID, time, and
 *   computed hash.
 *
 * @return mixed
 *   Return the API response on success, FALSE on failure.
 */
function _collegiatelink_api_get_request($post_data, $resource) {
  $base_url = variable_get('collegiatelink_api_url', '');

  if ($base_url === '') {
    return FALSE;
  }

  $query_string = http_build_query($post_data, '', '&');
  $api_query = sprintf('%s%s?%s', $base_url, $resource, $query_string);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  curl_setopt($ch, CURLOPT_URL, $api_query);
  curl_setopt($ch, CURLOPT_HTTPGET, TRUE);

  $response = curl_exec($ch);

  curl_close($ch);

  return $response;
}
