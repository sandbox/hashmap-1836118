<?php

/**
 * @file
 * Admin file for the CollegiateLink Drupal module.
 */


/**
 * Settings form for the CollegiateLink API module.
 *
 * @return mixed
 *   A form array.
 */
function collegiatelink_api_settings_form() {
  $form = array();

  $form['collegiatelink_api_url'] = array(
    '#title' => t('CollegiateLink API URL'),
    '#type' => 'textfield',
    '#description' => t('ex. https://collegiatelink.your-school.edu/ws'),
    '#default_value' => variable_get('collegiatelink_api_url', ''),
  );

  $form['collegiatelink_api_per_server_settings'] = array(
    '#title' => t('API Settings (Per Server)'),
    '#type' => 'textarea',
    '#description' => t('hostname|ip|api key|shared key'),
    '#default_value' => variable_get('collegiatelink_api_per_server_settings', ''),
  );

  return system_settings_form($form);
}
