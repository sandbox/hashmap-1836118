<?php

/**
 * @file
 * Status pages for the CollegiateLink API module.
 */

/**
 * Indicate API connection status, and total organization and event count.
 *
 * @return string
 *   HTML for the status page.
 */
function collegiatelink_api_status_page() {
  $request_params = _collegiatelink_api_get_request_params();
  $request_params['page'] = 1;
  $request_params['pagesize'] = 1;

  $response = _collegiatelink_api_get_request($request_params, CollegiateLinkApiResource::ORGANIZATION_LIST);
  $response_xml = simplexml_load_string($response);

  $html = '';

  if ($response_xml->results->error) {
    drupal_set_message(t('Could not connected to the CollegiateLink API!'), 'error');
  }
  else {
    drupal_set_message(t('Successfully connected to the CollegiateLink API.'));

    // Display the total number of organizations.
    $organizations = collegiatelink_api_get_organization_list();

    if ($organizations) {
      $organization_count = count($organizations);
      $msg = t('Organization Count: %organization_count', array('%organization_count' => $organization_count));
      $html .= sprintf('<div>%s</div>', $msg);
    }

    // Display the total number of events.
    $events = collegiatelink_api_get_complete_event_list();

    if ($events) {
      $event_count = count($events);
      $msg = t('Event Count: %event_count', array('%event_count' => $event_count));
      $html .= sprintf('<div>%s</div>', $msg);
    }
  }

  return $html;
}

/**
 * Show a table of all organizations.
 *
 * @return string
 *   HTML for the organizations page.
 */
function collegiatelink_api_status_organizations_page() {
  $organizations = collegiatelink_api_get_organization_list();

  if ($organizations === FALSE) {
    drupal_set_message(t('Could not get organizations.'), 'error');

    // Setting the organizations array to an empty array will allow the table
    // to be shown with no rows.
    $organizations = array();
  }

  $html = '';
  $organization_count = count($organizations);
  $msg = t('Organization Count: %organization_count', array('%organization_count' => $organization_count));
  $html .= sprintf('<div>%s</div>', $msg);

  $rows = array();

  foreach ($organizations as $organization) {
    $row['name'] = $organization->name;
    $row['type'] = $organization->type;
    $row['status'] = $organization->status;
    $rows[] = $row;
  }

  $table_variables = array(
    'header' => array('Name', 'Type', 'Status'),
    'rows' => $rows,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => array(),
    'sticky' => FALSE,
    'empty' => t('No results found.'),
  );

  $html .= theme_table($table_variables);

  return $html;
}

/**
 * Show a table of all events.
 *
 * @return string
 *   HTML for the events page.
 */
function collegiatelink_api_status_events_page() {
  $events = collegiatelink_api_get_complete_event_list();

  if ($events === FALSE) {
    drupal_set_message(t('Could not get events.'), 'error');

    // Setting the events array to an empty array will allow the table
    // to be shown with no rows.
    $events = array();
  }

  $html = '';
  $event_count = count($events);
  $msg = t('Event Count: %event_count', array('%event_count' => $event_count));
  $html .= sprintf('<div>%s</div>', $msg);

  $rows = array();

  foreach ($events as $event) {
    // Convert the dates to seconds from milliseconds.
    $start_date = (int) drupal_substr($event->startDate, 0, -3);
    $end_date = (int) drupal_substr($event->endDate, 0, -3);

    $row['name'] = $event->name;
    $row['start_date'] = date('F j, Y, g:i a', $start_date);
    $row['end_date'] = date('F j, Y, g:i a', $end_date);
    $row['organization'] = $event->organization->name;
    $rows[] = $row;
  }

  $table_variables = array(
    'header' => array('Name', 'Start date', 'End date', 'Organization'),
    'rows' => $rows,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => array(),
    'sticky' => FALSE,
    'empty' => t('No results found.'),
  );

  $html .= theme_table($table_variables);

  return $html;
}
