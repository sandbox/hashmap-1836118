-------------------------------------------------------------------------------
Summary
-------------------------------------------------------------------------------
Exposes the CollegiateLink API to Drupal.

The following CollegiateLink API resources are accessible:
* organization/list
* organization/roster
* user/memberships
* user/position
* event/list

-------------------------------------------------------------------------------
Settings
-------------------------------------------------------------------------------
Modules settings can be configured at /admin/config/collegiatelink-api

CollegiateLink API URL is specific to your organization. It should your
organization's CollegiateLink implementation, followed by "/ws" at the end of
the URL. It should be something like https://collegiatelink.your-school.edu/ws

CollegiateLink API keys are issued on a per-server basis. If you have multiple
web servers you will need an API key for all of them. The CollegiateLink API
modules supports environments that have multiple servers. In the API Settings
textarea, enter all information for a single server on one line in this form:
server1.example.com|server1's IP|server1's API Key|server1's Shared Key

Each additional server should be on its own line. It should look something like
this:
server1.example.com|server1's IP|server1's API Key|server1's Shared Key
server2.example.com|server2's IP|server2's API Key|server2's Shared Key
...
serverN.example.com|serverN's IP|serverN's API Key|serverN's Shared Key

-------------------------------------------------------------------------------
Usage
-------------------------------------------------------------------------------

-----------------------------------------------------------
Events
-----------------------------------------------------------
// Retrieves all events.
$events = collegiatelink_api_get_complete_event_list();

// Retrieves a list of events in a given date range.
$start_date = strtotime('2012-11-01 00:00:00');
$end_date = strtotime('2012-11-05 23:59:59');
$events = collegiatelink_api_get_event_list($start_date, $end_date)

-----------------------------------------------------------
Users
-----------------------------------------------------------
// Retrieves a list of the organizations that a user is a member of.
$collegiatelink_user_id = 123456;
$users = collegiatelink_api_get_user_membership($collegiatelink_user_id);

// Retrieves a list of the users who occupy a position based on a position
template in the various organizations.
$position_template_name = 'President';
$users = collegiatelink_api_get_user_position($position_template_name);

-----------------------------------------------------------
Organizations
-----------------------------------------------------------
// Retrieves a list of all organizations regardless of status.
$organizations = collegiatelink_api_get_organization_list();

// Retrieves a list of all the members for the specified organization.
$organization_id = 123456;
$members = collegiatelink_api_get_organization_roster($organization_id);
