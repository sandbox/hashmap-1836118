<?php

/**
 * @file
 * CollegiateLink API resource constants.
 */

class CollegiateLinkApiResource {
  const ORGANIZATION_LIST = '/organization/list';
  const ORGANIZATION_ROSTER = '/organization/roster';
  const USER_MEMBERSHIP = '/user/memberships';
  const USER_POSITION = '/user/position';
  const EVENT_LIST = '/event/list';
}
